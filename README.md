# OpenML dataset: Pakistans-Migration-History

https://www.openml.org/d/43698

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Every year a lot of people migrate to different countries from Pakistan and a lot of them migrate to Pakistan as emigrants of refugees, Pakistan ranks 2nd, according to UNHCR, among the countries to host the most refugees. Thus this is a tribute to Pakistan and an information to the world that Pakistan is quite different than you think!  
Content
Every year people either enter or leave Pakistan to settle down and start their new life. Negative here means people are entering Pakistan more than exiting. Positive otherwise,
Acknowledgements
A lot of entities have contributed and my major citation is "World Bank"!
Inspiration
This could be a very good data for people analyzing the population of Pakistan as a whole!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43698) of an [OpenML dataset](https://www.openml.org/d/43698). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43698/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43698/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43698/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

